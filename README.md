#Storage Manager
--------------------------------------------------------------------------------------
##Members
--------------------------------------------------------------------------------------
- [Abdulwahab Alothaim](aalothaim@hawk.iit.edu)
- [Yitao Ma](yma48@hawk.iit.edu)
- [Li Huang](lhuang29@hawk.iit.edu)
- [Sawsane Ouchtal](souchtal@hawk.iit.edu)

---------------------------------------------------------------------------------------
##File list 
---------------------------------------------------------------------------------------
  - dberror.c
  - dberror.h
  - storage_mgr.c
  - storage_mgr.h
  - test_assign1_1.c
  - test_helper.h
  - README.txt
  
  
----------------------------------------------------------------------------------------
##Installation instruction
----------------------------------------------------------------------------------------





-----------------------------------------------------------------------------------------
##Function descriptions
------------------------------------------------------------------------------------------
###Manipulate page file functions
-------------------------------------
`initStorageManager()` initializes the storage manager

`createPageFile()` creates a new page file fileName, the initial file size should be one page and fill this single page with '\0' bytes.

`openPageFile()` method opens an existing page file.

`closePageFile()` closed an open file.

`destroyPageFile()` method destroy(delete) a page file. 

---------------------------------------
###Reading blocks from disc functions:
---------------------------------------

`readBlock()` is for getting block position. Check if the file has less than pageNum pages or more than totalNumPages first.If it does,the method should return RC_READ_NON_EXISTING_PAGE.

`getBlockPos()` returns the current page position in a file

`readFirstBlock()` reads the first respective last page in a file

`readPreviousBlock()` reads the previous to the curPagePos of the file. The curPagePos should be moved to the page that was read. If the user tries to read a block before the first page,the method should return RC_READ_NON_EXISTING_PAGE.

`readCurrentBlock()` reads the current page of the file. The curPagePos should be moved to the page that was read.

`readNextBlock()` reads the next page relative to the curPagePos of the file. If the user tries to read a block after the last page of the file, the method should return RC_READ_NON_EXISTING_PAGE.

---------------------------------------
###Writing blocks to a page file functions
---------------------------------------

`writeBlock()` writes a page to disk using an absolute position.

`writeCurrentBlock()` writes a page to disk using the current position.

`appendEmptyBlock()` increases the number of pages in the file by one. The new last page should be filled with zero bytes.

`ensureCapacity()` If the file has less than numberOfPages pages then increase the size to numberOfPages.

-------------------------------------------------------------------------------------------------
##Test cases: of all additional test cases added 
-------------------------------------------------------------------------------------------------
###test case in test_assign1_1.c
-------------------------------------------------------------------------------------------------
- testCreateOpenClose()

- test createPageFile()
    - openPageFile
    - closePageFile
    - destroyPageFile
    
- testSinglePageContent()
    - createPageFile
    - openPageFile
    - readFirstBlock
    - writeBolock
    - readFirstBlock
    - DestroyPageFile 


